var _userData;
var _gameID = getParameterFromURL ( 'gameId' );//1005;
var _currencyId = getParameterFromURL ( 'currencyId' );
var isMuted = false
var _tokensToPlay = 0;
var __tokensResponse = null;
var __startButtonPressed = false;
var hit, whoosh, prize, crowd, pregame, theme, hooray;

function init(){
    hit = document.getElementById('hit');
    prize = document.getElementById('prize');
    crowd = document.getElementById('crowd');
    whoosh = document.getElementById('whoosh');
    pregame = document.getElementById('pregame');
    theme = document.getElementById('theme');
    hooray = document.getElementById('hooray');

    setupLayerClass()
    pregame.play()
    pregame.loop = true

    $('.about').click(function(){
        $('#instruction').css({zIndex: 102, display: 'flex'})
    })

    $('.instructions-image').click(function(){
        $('#instruction').css({zIndex: -199})
    })

    $('.sound-control').click(function(){
        isMuted = !isMuted
        setSoundSettings()
    })

}

function setupLayerClass() {

    var maxHeight = (window.innerHeight > 720) ? 720 : window.innerHeight
    var maxWidth = maxHeight == window.innerHeight ? ($(window).height() * 720 / 720) : 720
    $('.layer, .super-container').css({
        height: ($('.layer').width() * 720 / 720) + "px",
        maxHeight: maxHeight + "px",
        maxWidth: maxWidth + "px"
    })

    var startButton = {
        container: document.getElementById("start-button"),
        renderer: 'svg',
        loop: false,
        autoplay: false,
        rendererSettings: {
            progressiveLoad:false
        },

        animationData: jsons.startButton
    }
    var startButtonAnim = bodymovin.loadAnimation(startButton)

    if(window.location.href.match("no-init") != null){
        isMuted = (getParameterFromURL('isMuted') == 'true');
        setSoundSettings()
        startButtonPressed(startButtonAnim)
    } else {
        $('#start-button').click(function(){
            $(this).addClass('disabled')
            startButtonPressed(startButtonAnim)
        })
    }

}

function startButtonPressed(animation) {
    animation.play()
    hit.play()
    initializeGame()

    $('.draggable').draggable({
        axis: "x",
        containment: "#bar",
        start: function() {
            updateDraggableValue($('.draggable').css('left'))
        },
        drag: function() {
            updateDraggableValue($('.draggable').css('left'))
        },
        stop: function() {
            updateDraggableValue($('.draggable').css('left'))
        }
    });

    $('#bar').click(function(e){
        var distanceA = ($('body').width()/2) - ($('.layer').width()/2)
        var distanceB = ($('.layer').width()/2) - ($('#token-ui').width()/2)
        var distanceC = ($('#token-ui').width()/2) - ($('#bar').width()/2)
        var value = e.clientX - (distanceA + distanceB + distanceC) - ($('.draggable').width()/2)
        $('.draggable').css({left: value})
        updateDraggableValue($('.draggable').css('left'))
    })

    $( document ).trigger ( 'games:onGetUserInformation', [ _gameID, _currencyId ] );

}

function playButtonClick () {
    var playButton = {
        container: document.getElementById("play-button"),
        renderer: 'svg',
        loop: false,
        autoplay: false,
        rendererSettings: {
            progressiveLoad:false
        },

        animationData: jsons.playButton
    }

    var playButtonAnim = bodymovin.loadAnimation(playButton);

    $(this).addClass('disabled')
    hit.play()
    playButtonAnim.play()
    _tokensToPlay = parseInt( $('#tokens-amount').text() )

    $( document ).trigger ( 'games:onPlayGame', [ _gameID, _currencyId, _tokensToPlay ] );

}

function setupFinalScreen() {

    hooray.play()
    whoosh.play()
    theme.pause()
    if(__tokensResponse.playId == 10) crowd.play()

    $('#final-screen').css({display: 'flex'})
    var final = {
        container: document.getElementById("final-screen"),
        renderer: 'svg',
        loop: true,
        autoplay: false,
        rendererSettings: {
            progressiveLoad:false
        },

        animationData: jsons.winning
    }

    jsons.winning.layers[0].ef[__tokensResponse.playId - 1].ef[0].v.k = 1
    jsons.winning.layers[13].t.d.k[0].s.t = ""+_tokensToPlay
    jsons.winning.layers[14].t.d.k[0].s.t = ""+__tokensResponse.totalPoints

    var finalAnim = bodymovin.loadAnimation(final)
    finalAnim.play()

    $('#final-screen').click(function() {
        $(this).addClass('disabled')
        hit.play()
        setTimeout(function(){
            var mainURL = location.protocol + '//' + location.host + location.pathname
            window.location.href = mainURL+ window.location.search + "&no-init=true&isMuted=" + isMuted
        },500)
    })

    $(document).trigger('games:onUpdateBalances');

}

function getRandomIndex() {
    return  Math.floor((Math.random() * 18) + 1);
}

function updateDraggableValue(val){
    var value = parseInt(val)
    var barWidth = $('#bar').width() - ($('.draggable').width() * 2) * 0.7
    var newVal = (value * 250 / barWidth)

    if(newVal > 250){
        newVal = 250
    }

    if(newVal < 1) newVal = 1
    if(newVal > _userData.tokenBalance) newVal = _userData.tokenBalance
    $('#tokens-amount').text(parseInt(newVal))
}

function setSoundSettings() {
    if(!isMuted) {$('.sound-control').attr('src','./assets/img/sound_on.png')}
    else {$('.sound-control').attr('src','./assets/img/sound_off.png')}

    hit.volume = (isMuted) ? 0 : 1
    prize.volume = (isMuted) ? 0 : 1
    crowd.volume = (isMuted) ? 0 : 1
    whoosh.volume = (isMuted) ? 0 : 1
    pregame.volume = (isMuted) ? 0 : 1
    theme.volume = (isMuted) ? 0 : 1
    hooray.volume = (isMuted) ? 0 : 1
}

function afterUserInfo ( userData ) {

    _userData = userData;

    if (_userData.tokenBalance == 0){
        $('#about').fadeOut();
        $('#no-tokens-screen').fadeIn();
    }
    if(_userData.status == 0){
        $('#about').fadeOut()
        $('#t-s').text(_userData.message)
        $('#no-server-request').fadeIn()
    }
    $('#start-screen').fadeOut()

    jsons.balance.layers[0].t.d.k[0].s.t = ""+_userData.pointBalance
    jsons.balance.layers[1].t.d.k[0].s.t = ""+_userData.tokenBalance

    var balance = {
        container: document.getElementById("balances-container"),
        renderer: 'svg',
        loop: false,
        autoplay: false,
        rendererSettings: {
            progressiveLoad:false
        },

        animationData: jsons.balance
    }
    var balanceAnim = bodymovin.loadAnimation(balance)
    balanceAnim.play()




    $('#play-button').click( playButtonClick );

}

function afterPlay ( data ) {

    __tokensResponse = data;
    $('#token-screen').fadeOut()
    __startButtonPressed = true
    pregame.pause()
    theme.play()
    theme.loop = true

}