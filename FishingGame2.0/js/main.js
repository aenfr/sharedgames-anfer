var _userData;
//Game Core Id: 1018
var _gameID = getParameterFromURL ( 'gameId' );
var _currencyId = getParameterFromURL( 'currencyId' );
var isMuted = false
var whoosh, prize, crowd, preSound, soundMusic, hit;
var selectedAnim;
var _tokensToPlay = 1
var gamePlay;
var overlay;
var lure1;
var lure10;
var lure50;
var lure250;
var lureTitle;
var fishButton;
var overlayAnim;
var gamePlayAnim;
var fisherman;
var fishermanAnim;
var lure1Anim;
var unserseaAnim;
var unsersea;
var lure50Anim;
var lure250Anim;
var lureTitleAnim;
var finalPlayed = false

var gameJSONs = gamejsons

function init(){
    
    whoosh      = document.getElementById('whoosh');
    prize       = document.getElementById('prize');
    crowd       = document.getElementById('crowd');
    preSound    = document.getElementById('pre-sound');
    soundMusic  = document.getElementById('sound-music');
    hit         = document.getElementById('hit');

    preSound.loop = true
    preSound.play()

    $('.layer').css({
	    height: ($('.layer').width() * 650 / 750) + "px", 
        maxHeight: window.innerHeight + "px",
        maxWidth: ($(window).height() * 750 / 650) + "px"
    })

    $('.super-container').css({
        maxWidth: ($(window).height() * 750 / 650) + "px"
    })

    $( document ).trigger ( 'games:onGetUserInformation', [ _gameID, _currencyId ] );

    $('.sound-toggle').click(function(){
        hit.play()
        isMuted = !isMuted
        setSoundSettings()
    })

    var startButton = {
        container: document.getElementById("start-button"),
        renderer: 'svg',
        loop: false,
        autoplay: false,
        rendererSettings: {
            progressiveLoad:false
        },

        animationData: gameJSONs.startButton
    }
    var startButtonAnim = bodymovin.loadAnimation(startButton)
    startButtonAnim.playSegments([0, 1], true)

    if(window.location.href.match("no-init") != null){
        isMuted = (getParameterFromURL('isMuted') == 'true');
        setSoundSettings()
        startButtonAnim.playSegments([1, 10], true)
        $('#init-screen').fadeOut(function(){
            setupInitLayers()
        })
    }

    $('#start-button').click(function(){
        $(this).addClass('disabled')
        hit.play()
        $('#bar').css({zIndex: 102})
        startButtonAnim.playSegments([1, 10], true)
        $('#init-screen').fadeOut(function(){
            setupInitLayers()
        })
    })

    $('.draggable').draggable({ 
        axis: "x",
        containment: "#bar",
        start: function() {
            updateDraggableValue($('.draggable').css('left'))
        },
        drag: function() {
            updateDraggableValue($('.draggable').css('left'))
        },
        stop: function() {
            updateDraggableValue($('.draggable').css('left'))
        }
    });

    $('#bar').click(function(e){
        var distanceA = ($('body').width()/2) - ($('.layer').width()/2)
        var distanceB = ($('.layer').width()/2) - ($('#token-ui').width()/2)
        var distanceC = ($('#token-ui').width()/2) - ($('#bar').width()/2)
        var value = e.clientX - (distanceA + distanceB + distanceC) - ($('.draggable').width()/2)
        $('.draggable').css({left: value})
        updateDraggableValue($('.draggable').css('left'))
    })

}

function updateDraggableValue(val){
    var value = parseInt(val)
    var barWidth = $('#bar').width() - $('.draggable').width()
    var newVal = value * 250 / barWidth
    
    if(newVal > 250){ 
        newVal = 250
    }

    if(newVal < 1) newVal = 1
    if(newVal > _userData.tokenBalance) newVal = _userData.tokenBalance
    _tokensToPlay = parseInt(newVal)
    $('#tokens-amount').text(parseInt(newVal))
}

function setUpWinning(){
    if(!finalPlayed) {
        finalPlayed = true
        fishermanAnim.playSegments([290, 420], true)

        $('.final-points').css({opacity: 1})

        gameJSONs.winning.layers[3].t.d.k[0].s.t = __tokensResponse.totalPoints+""
        gameJSONs.winning.layers[4].t.d.k[0].s.t = _tokensToPlay+""
        
        winning = {
            container: document.getElementById("winning-screen"),
            renderer: 'svg',
            loop: false,
            autoplay: false,
            rendererSettings: {
                progressiveLoad:false
            },

            animationData: gameJSONs.winning
        }
        winningAnim = bodymovin.loadAnimation(winning)
        winningAnim.playSegments([0, 350], true)
        
        againButton = {
            container: document.getElementById("play-again"),
            renderer: 'svg',
            loop: false,
            autoplay: false,
            rendererSettings: {
                progressiveLoad:false
            },

            animationData: gameJSONs.playAgain
        }
        var againButton = bodymovin.loadAnimation(againButton)
        againButton.playSegments([0, 1], true)

        $('#play-again').click(function(){
            $(this).addClass('disabled')
            hit.play()
            setTimeout(function(){
                var mainURL = location.protocol + '//' + location.host + location.pathname
                window.location.href = mainURL+ window.location.search+"&no-init=true&isMuted=" + isMuted
            },500)
        })                        
    }

    $(document).trigger('games:onUpdateBalances');

}

function setupInitLayers(){

    fisherman = {
        container: document.getElementById("fisherman"),
        renderer: 'svg',
        loop: false,
        autoplay: false,
        rendererSettings: {
            progressiveLoad:false
        },

        animationData: gameJSONs.fisherman
    }

    fishermanAnim = bodymovin.loadAnimation(fisherman)
    fishermanAnim.playSegments([0, 1],true);

    undersea = {
        container: document.getElementById("undersea"),
        renderer: 'svg',
        loop: true,
        autoplay: true,
        rendererSettings: {
            progressiveLoad:false
        },

        animationData: gameJSONs.undersea
    }

    unserseaAnim = bodymovin.loadAnimation(undersea)
    unserseaAnim.play()

    var fisherButton = {
        container: document.getElementById("fisher-button"),
        renderer: 'svg',
        loop: false,
        autoplay: false,
        rendererSettings: {
            progressiveLoad:false
        },

        animationData: gameJSONs.fishButton
    }

    var fisherButtonAnim = bodymovin.loadAnimation(fisherButton)
    fisherButtonAnim.playSegments([0, 2], true)

    $('#fisher-button, #play-button').click(function(){
        fisherButtonAnim.playSegments([3, 13], true)
        $(this).addClass('disabled')
        hit.play()

        $( document ).trigger ( 'games:onPlayGame', [ _gameID, _currencyId, _tokensToPlay ] );

    })

}

function setSoundSettings() {
    if(!isMuted) {$('.sound-toggle').attr('src','./assets/img/sound_on.png')}
    else {$('.sound-toggle').attr('src','./assets/img/sound_off.png')}

    whoosh.volume      = (isMuted) ? 0 : 1
    prize.volume       = (isMuted) ? 0 : 1
    crowd.volume       = (isMuted) ? 0 : 1
    preSound.volume    = (isMuted) ? 0 : 1
    soundMusic.volume  = (isMuted) ? 0 : 1
    hit.volume         = (isMuted) ? 0 : 1
}

function afterUserInfo ( userData ) {

    _userData = userData;

    if (_userData.tokenBalance == 0)
        $('#no-tokens-screen').css({display: 'block', zIndex: 999})

    $("p.points-container").text(_userData.pointBalance)
    $("p.tokens-container").text(_userData.tokenBalance)

}

function afterPlay ( data ) {

    preSound.pause()
    soundMusic.play()

    var playID = data.playId
    __tokensResponse = data;
    gameJSONs.winning.layers[1].ef[playID-1].ef[0].v.k = 1

    $('#init-animations').fadeOut()

    gameplays = {
        container: document.getElementById("gameplays"),
        renderer: 'svg',
        loop: false,
        autoplay: false,
        rendererSettings: {
            progressiveLoad:false
        },

        animationData: gameJSONs.gameplays[playID-1]
    }
    gameplaysAnim = bodymovin.loadAnimation(gameplays)
    gameplaysAnim.play()

    fishermanAnim.playSegments([0, 289], true)
    fishermanAnim.addEventListener("complete", function() {

        setUpWinning()
        onInactive(function(){
            setUpWinning()
        })
    });

}