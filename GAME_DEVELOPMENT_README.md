# Snowfly Game Development #

In an attempt to streamline game development, testing and propagation, I will be outlining a process to follow during 
the game development life cycle in this file. This should make development and testing quicker, for which I'm sure all 
parties will be grateful. So I will give some basic structures for the games some of which is abstract and some concrete.
Keep in mind that this will be an ongoing project as we attempt to get creativity, uniformity and simplicity into the games.

### Driving Concepts ###

Snowfly Games drive the connection between behavior and reward. In principle the game will give a budget driven, random, 
output of a currency for a good behavior that they have achieved, and that their employer has deemed rewardable. The premise
is that a user will find excitement behind game theory practice; meaning that the user will perceive that receiving a dynamic
amount of output for a single input is better than receiving a static amount consistently. This game theory in conjunction
with behavioral modification is what drives the core of Snowfly.

Games have to meet client exceptions on average duration of 'play'. As these games will be used in corporate workflows and
during office hours, Snowfly has given the expectation that games will be short, engaging, and not skill based. Game plays 
should average between 10-20 seconds. A good game will allow a user to interact with it, but the interactions should not 
allow the user to become 'better' and encourage the user to try to become better at the game. The game should be exciting, 
the most exciting should be winning higher plays which means more money.

### Game Structure Overview ###

A game will consist of a couple basic 'parts':

1. Start Screen
    * The start screen will be the screen that all users will see having loaded the game. It should be fairly simple
    with an image of the game as the background, a start button, and perhaps if necessary a summery or short description.
2. Selection Screen
    * The selection screen should include:
        1. The users token balance
        2. The point balance
        3. A means of selecting an amount
3. A game play
    * the game play will be variable depending on the data returned from the play game endpoint. A play will be in the 
    most basic form a series of animations, and in a more interactive form a way for a user to engage before an animation
    that will show the results.
4. Score Screen
    * the score screen should show the results of the game play. There are 10 returned play levels, 10 being a jackpot,
    and 1 being the lowest win you can get. The level are arbitrary numbers assigned to play to indicate the value of a 
    win in simple terms.

### Getting Started ###

Before you start development you will need to request an Identifier and submit a game abstract and name to Snowfly to attach to your game. This will just
This id will be attached to your game and will identify the game to snowfly and will make it available after game approval
to be used in the Snowfly system and system extensions.

### Start Screen ###

The start screen should be fun graphic with a start button and perhaps music (that can be muted), that will stay until 
the user decides to proceed.  

### ... ###

More to follow.