// Global variables
var __gameCoreId = 1009;
var _currencyId = getParameterFromURL( 'currencyId' );
var __gameId = getParameterFromURL('gameId');
var __userData,
    __tokensToPlayJSON,
    __tokensToPlay,
    __playId,
    __index,
    GAMEP_VEL = 20,
    UI_VEL = 15,
    n;
var dragButton,
    soundMusic,
    playButton,
    dynamicText,
    style;
var calledAPI = false;
var initSound;
var game = new Phaser.Game(560, 400, Phaser.CANVAS, 'Dart');

//Preload 'loading' images
var bootState = {

    preload: function(){
        this.load.image('loading_bar', './assets/img/loading_bar.png');
        this.load.image('loading_word', './assets/img/loading_word.png');
        this.load.spritesheet('spinner', './assets/sprites/loading-spinner/loading-spinner.png', 560, 400, 45);
        game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        // Have the game centered horizontally
        game.scale.pageAlignHorizontally = true;
        // And vertically
        game.scale.pageAlignVertically = true;
    },
    create: function(){
        //moving to the loaderState
        game.state.start('loaderState');

    }

};

var loaderState = {

    preload: function(){

        //showing loaging images
        var spinner = this.add.sprite(0, 0, 'spinner');
        spinner.animations.add('spinning');
        spinner.animations.play('spinning', 20, true);
        this.add.image(game.width/2 - 33, game.height/2 + 110, 'loading_word');
        var loadingBar = this.add.image(game.width / 2, game.height/2 + 100, 'loading_bar');
        loadingBar.anchor.setTo(0.5,0.5);
        this.load.setPreloadSprite(loadingBar);

        //loading static images
        this.load.image('layer', './assets/img/opacity.png');
        this.load.image('token_ui', './assets/img/token-ui.png');
        this.load.image('drag_button', './assets/img/drag_button.png');
        this.load.image('sound_on', './assets/img/sound_on.png');
        this.load.image('sound_off', './assets/img/sound_off.png');
        this.load.image('bar', './assets/img/bar.png');
        this.load.image('points', './assets/img/points.png');
        this.load.image('nogametokens','assets/nogametokens.png');
        this.load.image('bg', './assets/img/bg.jpg');
        this.load.image('stadium', './assets/img/stadium.jpg');
        this.load.image('versus', './assets/img/versus.png');

        //Loading sprites
        this.load.spritesheet('start_button', './assets/sprites/start_button/start_button.png', 97, 49, 40);
        this.load.spritesheet('play_button', './assets/sprites/play_button/play_button.png', 52, 52, 11);
        this.load.spritesheet('number', './assets/sprites/numbers/numbers.png', 38, 40, 10);
        this.load.spritesheet('again_button', './assets/sprites/play_again_button/again.png', 147, 33, 10);
        this.load.spritesheet('expert', './assets/sprites/expert/expert.png', 40, 15, 10);
        this.load.spritesheet('newbie', './assets/sprites/newbie/newbie.png', 42, 15, 10);
        this.load.spritesheet('rookie', './assets/sprites/rookie/rookie.png', 40, 15, 10);
        this.load.spritesheet('skilled', './assets/sprites/skilled/skilled.png', 42, 16, 10);
        this.load.spritesheet('number', './assets/sprites/final_animations/numbers/n.png', 51, 59, 10);

        this.load.spritesheet('blue-goal', './assets/sprites/postshots/blue-goal.jpg', 560, 400, 64);
        this.load.spritesheet('blue-miss', './assets/sprites/postshots/blue-miss.jpg', 560, 400, 64);
        this.load.spritesheet('red-goal', './assets/sprites/postshots/red-goal.jpg', 560, 400, 64);
        this.load.spritesheet('red-miss', './assets/sprites/postshots/red-miss.jpg', 560, 400, 64);
        this.load.spritesheet('blue-preshot', './assets/sprites/preshots/blue-preshot.jpg', 560, 400, 40);
        this.load.spritesheet('red-preshot', './assets/sprites/preshots/red-preshot.jpg', 560, 400, 40);
        this.load.spritesheet('blue-miss-2', './assets/sprites/blue-miss-2/blue-miss-2.jpg', 560, 400, 40);
        this.load.spritesheet('snowfly-logo_1', './assets/sprites/logos/snowfly/logo1.png', 560, 400, 77);
        this.load.spritesheet('snowfly-logo_2', './assets/sprites/logos/snowfly/logo2.png', 560, 400, 77);
        this.load.spritesheet('title', './assets/sprites/title/title.png', 316, 196, 40);
        this.load.spritesheet('blueBar', './assets/sprites/bar/blueTeam/blueBar.png', 172.5, 82.5, 16);
        this.load.spritesheet('redBar', './assets/sprites/bar/redTeam/redBar.png', 172.5, 82.5, 16);
        this.load.spritesheet('token-panel', './assets/sprites/token-panel/token-panel.png', 180, 79, 20);
        this.load.spritesheet('game1', './assets/sprites/shotLabels/game1.png', 224, 140, 36);
        this.load.spritesheet('game2', './assets/sprites/shotLabels/game2.png', 224, 140, 36);
        this.load.spritesheet('game3', './assets/sprites/shotLabels/game3.png', 224, 140, 36);
        this.load.spritesheet('confetti', './assets/sprites/final_animations/confetti/confetti.png', 560, 400, 30);
        this.load.spritesheet('turn-1', './assets/sprites/turns/your-turn.png', 224, 140, 36);
        this.load.spritesheet('turn-2', './assets/sprites/turns/opponents-turn.png', 224, 140, 36);

        this.load.spritesheet('1-blue-goal', './assets/sprites/bar/blueTeam/1blueGoal.png', 172.5, 82.5, 4);
        this.load.spritesheet('2-blue-goal', './assets/sprites/bar/blueTeam/2blueGoal.png', 172.5, 82.5, 4);
        this.load.spritesheet('3-blue-goal', './assets/sprites/bar/blueTeam/3blueGoal.png', 172.5, 82.5, 4);
        this.load.spritesheet('1-blue-miss', './assets/sprites/bar/blueTeam/1blueMiss.png', 172.5, 82.5, 4);
        this.load.spritesheet('2-blue-miss', './assets/sprites/bar/blueTeam/2blueMiss.png', 172.5, 82.5, 4);
        this.load.spritesheet('3-blue-miss', './assets/sprites/bar/blueTeam/3blueMiss.png', 172.5, 82.5, 4);

        this.load.spritesheet('1-red-goal', './assets/sprites/bar/redTeam/1redGoal.png', 172.5, 82.5, 4);
        this.load.spritesheet('2-red-goal', './assets/sprites/bar/redTeam/2redGoal.png', 172.5, 82.5, 4);
        this.load.spritesheet('3-red-goal', './assets/sprites/bar/redTeam/3redGoal.png', 172.5, 82.5, 4);
        this.load.spritesheet('1-red-miss', './assets/sprites/bar/redTeam/1redMiss.png', 172.5, 82.5, 4);
        this.load.spritesheet('2-red-miss', './assets/sprites/bar/redTeam/2redMiss.png', 172.5, 82.5, 4);
        this.load.spritesheet('3-red-miss', './assets/sprites/bar/redTeam/3redMiss.png', 172.5, 82.5, 4);

        //Loading sounds
        this.load.audio('pop', './assets/sounds/pop.mp3');
        this.load.audio('hockey-music', './assets/sounds/hockey-music.mp3');
        this.load.audio('main_sound', './assets/sounds/main_sound.mp3');
        this.load.audio('crowd', './assets/sounds/crowd.mp3');

    },
    create: function(){
        //moving to the first gameplay state
        game.state.start('initState');

    },
}

var initState = {

    numbersArray: [
    [0,1,2,3,4,5,6,7,8,9],
    [1,2,3,4,5,6,7,8,9,0],
    [2,3,4,5,6,7,8,9,0,1],
    [3,4,5,6,7,8,9,0,1,2],
    [4,5,6,7,8,9,0,1,2,3],
    [5,6,7,8,9,0,1,2,3,4],
    [6,7,8,9,0,1,2,3,4,5],
    [7,8,9,0,1,2,3,4,5,6],
    [8,9,0,1,2,3,4,5,6,7], 
    [9,0,1,2,3,4,5,6,7,8]
    ],

    prizesTable: [0, 2, 4, 8, 10, 25, 100, 250, 500, 2500, 5000],

    create: function(){
        
        this.stage.disableVisibilityChange = true
        game.add.image(0, 0, 'bg');

        soundMusic = game.add.audio('hockey-music');
        soundMusic.loop = true;
        soundMusic.play();

        var startButton = this.add.sprite(game.width / 2 - 50, 300, 'start_button');
        startButton.animations.add('init', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29]);
        startButton.animations.add('click', [29,30,31,32,33,34,35,36,37,38,39]);
        var startButtonInterval = setInterval(()=>{
            startButton.animations.play('init', 15, false);    
        }, 1500);

        var title = this.add.sprite(game.width/2 - 150, 70, 'title');
        title.animations.add('init');
        var titleInterval = setInterval(()=>{
            title.animations.play('init', 15, false);    
        }, 1500);

        startButton.inputEnabled = true;
        startButton.events.onInputDown.add(function(){
            startButton.animations.play('click', 15, false);
            setTimeout(function(){
                clearInterval(titleInterval);
                clearInterval(startButtonInterval);
                game.state.start('tokensState');
            }, 500);
        }, this);

        var sound_on = this.add.image(5, 5, 'sound_on');
        var sound_off = this.add.image(5, 20, 'sound_off');
        if(!game.sound.mute)sound_off.alpha = 0;
        else sound_on.alpha = 0;
        sound_on.inputEnabled = true;
        sound_on.events.onInputDown.add(function(target){
            game.sound.mute = true;
            sound_off.alpha = 1;
            target.alpha = 0;
        }, this);

        sound_off.inputEnabled = true;
        sound_off.events.onInputDown.add(function(target){
            game.sound.mute = false;
            sound_on.alpha = 1;
            target.alpha = 0;
        }, this);
    }
};

var tokensState = {

    create: function(){
        $(document).trigger('games:onGetUserInformation', [__gameId, _currencyId]);
    }
};

var gamePlayState = {

    numbersArray: [
    [0,1,2,3,4,5,6,7,8,9],
    [1,2,3,4,5,6,7,8,9,0],
    [2,3,4,5,6,7,8,9,0,1],
    [3,4,5,6,7,8,9,0,1,2],
    [4,5,6,7,8,9,0,1,2,3],
    [5,6,7,8,9,0,1,2,3,4],
    [6,7,8,9,0,1,2,3,4,5],
    [7,8,9,0,1,2,3,4,5,6],
    [8,9,0,1,2,3,4,5,6,7], 
    [9,0,1,2,3,4,5,6,7,8]
    ],

    prizesTable: [0, 2, 4, 8, 10, 25, 100, 250, 500, 2500, 5000],

    create: function(){
        if(calledAPI == false){
            $( document ).trigger ( 'games:onPlayGame', [ __gameId, _currencyId, parseInt(dynamicText._text) ] );
        }else{
            console.log('Error! Tried to call API more then once.');
            return;
        }
    }   
};

//setting all the game-states
game.state.add('loaderState', loaderState);
game.state.add('bootState', bootState);
game.state.add('tokensState', tokensState);
game.state.add('initState', initState);
game.state.add('gamePlayState', gamePlayState);
game.state.start('bootState');

var limitRight = 432;

var __dragged = false;
function updateDrag(){

    dragButton.position.y = 192 - 40;
    if(dragButton.position.x > limitRight)
        dragButton.position.x = limitRight;
    else if(dragButton.position.x < 92)
        dragButton.position.x = 92;

    var draggedVal;
    var position = dragButton.position.x;

    if(position >= 92 && position < (190))
        draggedVal = (((position - 92) * 11) / 98);
    else if(position >= 190 && position < (285))
        draggedVal = (((position - 190) * 32) / 98) + 10;
    else if(position >= (285))
        draggedVal = (((position - 285) * 140) / 98) + 40;

    if(position > 102) playButton.play('available');
    
    if(!__dragged){
        __dragged = true;
        playButton.inputEnabled = true;
        playButton.events.onInputDown.add(function(){
            if(position >= 92){
                playButton.play('clicked');
                setTimeout(function(){
                game.add.audio('pop').play();
                game.state.start('gamePlayState');  
              },1000)
            } 
        });        
    }

    if(draggedVal > __userData.tokenBalance){
        limitRight = position++;
        draggedVal = __userData.tokenBalance;
    }
    if(draggedVal < 1)draggedVal = 1;
    dynamicText.destroy();
    dynamicText = game.add.text(268, 122 - 40, parseInt(draggedVal), style);
}

var laps;
var __total;
var lapIndicator = 1;
var mainAudio;

function setAnimation(array, uiGroup, total){
    
    if(total){   
        laps = total/4;
        __total = total;
        var turn1 = game.add.sprite(game.width - 224, game.height - 140, 'turn-1');
        turn1.animations.add('moving')
        turn1.play('moving', UI_VEL, false)
        uiGroup.add(turn1)
    }

    if(array.length % 4 == 0){
        var logo = game.add.sprite(0, 0, 'snowfly-logo_1');
        logo.animations.add('moving');
        logo.animations.play('moving', GAMEP_VEL, false);
        uiGroup.add(logo);    
        logo.events.onAnimationComplete.add(()=>{
            logo.destroy();
        });
    }

    if(array.length == 7 || array.length == 3 || array.length == 11){
        var logo_2 = game.add.sprite(0, 0, 'snowfly-logo_2');
        logo_2.animations.add('moving');
        logo_2.animations.play('moving', GAMEP_VEL, false);
        uiGroup.add(logo_2);
        logo_2.events.onAnimationComplete.add(()=>{
            logo_2.destroy();
        });
    }

    if(array.length){
        key = array.pop();
        var item = game.add.sprite(0,0, key);
        item.animations.add('run');
        item.animations.play('run', GAMEP_VEL, false);
        game.world.bringToTop(uiGroup)

        if((__total-1) == array.length) lapIndicator = 1;

        if((__total-1) - 4 == array.length) lapIndicator = 2;

        if((__total-1) - 8 == array.length) lapIndicator = 3;

        

        if(key.split('-')[1] == 'preshot'){
            mainAudio = game.add.audio('main_sound').play();
        }

        item.events.onAnimationComplete.add(() => {

            if(key.split('-')[1] == 'preshot'){

                var newKey = array[array.length-1];

                var distance = (newKey.split('-')[0] == 'red') ? (game.width - 165) : 0;
                newKey = (newKey[newKey.length-1] == 2) ? newKey.substring(0, newKey.length-2) : newKey;
                var point = game.add.sprite(distance, 0, lapIndicator +'-'+ newKey);
                uiGroup.add(point);
                point.animations.add('show');
                setTimeout(()=>{
                    game.add.audio('pop').play();
                    point.animations.play('show', UI_VEL, false);
                },1000);
                
            }else{

                if(key.split('-')[0] == 'red'){
                    var turn1 = game.add.sprite(game.width - 224, game.height - 140, 'turn-1');
                    turn1.animations.add('moving')
                    turn1.play('moving', UI_VEL, false)
                    uiGroup.add(turn1)
                }
                else {
                    var turn2 = game.add.sprite(game.width - 224, game.height - 140, 'turn-2');
                    turn2.animations.add('moving')
                    turn2.play('moving', UI_VEL, false)
                    uiGroup.add(turn2)
                }
                mainAudio.stop();
            }

            if(array.length != 1) item.destroy();
            setAnimation(array, uiGroup);

        });


    }else{

        var final = game.add.sprite(0,0, key);
        final.animations.add('final', [63]);
        final.animations.play('final', UI_VEL, false);
        var layer = game.add.image(0,0,'layer');
        layer.alpha = 0;
        game.add.tween(layer).to({alpha: 1}, 800, Phaser.Easing.Linear.None, true, 0);

        var style = { font: "24px Adventure", fill: "#fff"};
        var text = game.add.text(80, game.height/2 - 40 - 40, 'TOKENS PLAYED', style);
        var text = game.add.text(350, game.height/2 - 40 - 40, 'POINTS WON', style);

        var againButton = game.add.sprite(game.width / 2 - 80, game.height - 68 - 20, 'again_button');
        againButton.animations.add('click');

        againButton.inputEnabled = true;
        againButton.events.onInputOver.add(function(){
            againButton.animations.add('hover', [4]);
            againButton.animations.play('hover');
        });

        againButton.events.onInputOut.add(function(){
            againButton.animations.add('no-hover', [0]);
            againButton.animations.play('no-hover');
        });

        againButton.events.onInputDown.add(function(){
            againButton.animations.play('click', UI_VEL, false);
            setTimeout(function(){
                soundMusic.loop = true;
                soundMusic.play();
                game.state.start('tokensState');
                try {
                    window.top.updateBalances();
                } catch ( exception ) {
                    console.log('unable to update balance.');
                }
                calledAPI = false;
            }, 500);
        }, this);

        var amount = dynamicText._text;
        var separation = amount.toString().length > 3 ? 14 : 24;
        var position = amount.toString().length > 3 ? [535 - 80, 125 + 80] : [510 - 80, 100 + 80];
        var n = ("" + amount).split("");
        for(var i = 0; i < amount.toString().length; i++){
            var number_two = gamePlayState.add.sprite((parseInt(i * separation) + position[1]) - (25 * (n.length)), game.world.centerY - 30, 'number');
            number_two.animations.add('animation', gamePlayState.numbersArray[parseInt(n[i])]);
            number_two.animations.play('animation', 20, false);
        }
        var tokensAmount = gamePlayState.prizesTable[__playId];
        var separation = amount.toString().length > 3 ? 14 : 24;
        var position = amount.toString().length > 3 ? [535 - 80, 125 + 80] : [510 - 80, 100 + 80];
        var n = ("" + tokensAmount).split("");
        for(var i = 0; i < tokensAmount.toString().length; i++){
            var number_one = gamePlayState.add.sprite((parseInt(i * separation) + position[0]) - (25 * (n.length)), game.world.centerY - 30, 'number');
            number_one.animations.add('animation', gamePlayState.numbersArray[parseInt(n[i])]);
            number_one.animations.play('animation', 20, false);
        }

        if(__playId == 10){
            game.add.audio('crowd').play();
            var confetti = game.add.sprite(0, 0, 'confetti');
            confetti.animations.add('confetti');
            confetti.animations.play('confetti', 25, false);
        }
        $(document).trigger('games:onUpdateBalances');
    }

}

function afterPlay ( data ) {

    __tokensToPlayJSON = data;
    calledAPI = true;
    __playId = data.playId;

    function setLabel(n){
        var gameLabel = game.add.sprite(game.width/2 - 112, game.height/2 - 70, 'game' + n);
        gameLabel.animations.add('run');
        gameLabel.animations.play('run', 15, false);
    }

    var redPreShot = 'red-preshot';
    var bluePreShot = 'blue-preshot';

    var blueMiss = 'blue-miss';
    var redMiss = 'red-miss';
    var blueMiss2 = 'blue-miss-2';

    var redGoal = 'red-goal';
    var blueGoal = 'blue-goal';

    var uiGroup = gamePlayState.add.group();
    var labels = gamePlayState.add.group();

    var sound_on = gamePlayState.add.image(5, game.height - 30, 'sound_on');
    uiGroup.add(sound_on);
    var sound_off = gamePlayState.add.image(25, game.height - 30, 'sound_off');
    uiGroup.add(sound_off);
    if(!game.sound.mute)sound_off.alpha = 0;
    else sound_on.alpha = 0;
    sound_on.inputEnabled = true;
    sound_on.events.onInputDown.add(function(target){
        game.sound.mute = true;
        sound_off.alpha = 1;
        target.alpha = 0;
    }, this);

    sound_off.inputEnabled = true;
    sound_off.events.onInputDown.add(function(target){
        game.sound.mute = false;
        sound_on.alpha = 1;
        target.alpha = 0;
    }, this);

    setTimeout(()=>{

        var blueBar = game.add.sprite(0, 0, 'blueBar');
        blueBar.animations.add('init', [0,1,2,3,4,5,6,7,8,9,10,11,12,13]);
        blueBar.animations.play('init', 15, false);

        var redBar = game.add.sprite(395, 0, 'redBar');
        redBar.animations.add('init', [0,1,2,3,4,5,6,7,8,9,10,11,12,13]);
        redBar.animations.play('init', 15, false);

        var tokenPanel = game.add.sprite(game.width/2 - 90, 0, 'token-panel');
        tokenPanel.animations.add('showing');
        tokenPanel.animations.play('showing', 15, false);
        style = { font: "20px Montserrat", fill: "#fff"};
        var tokens = game.add.text(game.width/2 - 58, 25, /*dynamicText.text*/__userData.tokenBalance, style);
        tokens.alpha = 0;
        var points = game.add.text(game.width/2 + 18, 25, /*__tokensToPlayJSON.totalPoints*/__userData.pointBalance, style);
        points.alpha = 0;

        uiGroup.add(tokenPanel)
        uiGroup.add(blueBar)
        uiGroup.add(redBar)
        uiGroup.add(tokens);
        uiGroup.add(points);

        game.add.tween(tokens).to({alpha: 1}, 1500, Phaser.Easing.Linear.None, true, 0);
        game.add.tween(points).to({alpha: 1}, 1500, Phaser.Easing.Linear.None, true, 0);

    },500)



    switch(__playId){
        case 1:

            var objectsArray = [redGoal, redPreShot, blueMiss, bluePreShot];
            setAnimation(objectsArray, uiGroup, objectsArray.length);

            break;
        case 2:

            var objectsArray = [redGoal, redPreShot, blueMiss, bluePreShot, redGoal, redPreShot, blueMiss, bluePreShot];
            setAnimation(objectsArray, uiGroup, objectsArray.length);

            break;
        case 3:

            var objectsArray = [redGoal, redPreShot, blueMiss, bluePreShot, redGoal, redPreShot, blueGoal, bluePreShot];
            setAnimation(objectsArray, uiGroup, objectsArray.length);

            break;
        case 4:

            var objectsArray = [redGoal, redPreShot, blueMiss2, bluePreShot, redMiss, redPreShot, blueGoal, bluePreShot];
            setAnimation(objectsArray, uiGroup, objectsArray.length);

            break;
        case 5:

            var objectsArray = [redMiss, redPreShot, blueMiss, bluePreShot];
            setAnimation(objectsArray, uiGroup, objectsArray.length);

            break;
        case 6:

            var objectsArray = [redGoal, redPreShot, blueGoal, bluePreShot, redGoal, redPreShot, blueGoal, bluePreShot];
            setAnimation(objectsArray, uiGroup, objectsArray.length);

            break;
        case 7:

            var objectsArray = [redMiss, redPreShot, blueGoal, bluePreShot, redMiss, redPreShot, blueMiss, bluePreShot];
            setAnimation(objectsArray, uiGroup, objectsArray.length);

            break;
        case 8:

            var objectsArray = [redMiss, redPreShot, blueMiss, bluePreShot, redMiss, redPreShot, blueGoal, bluePreShot];
            setAnimation(objectsArray, uiGroup, objectsArray.length);

            break;
        case 9:

            var objectsArray = [redMiss, redPreShot, blueGoal, bluePreShot, redGoal, redPreShot, blueGoal, bluePreShot]
            setAnimation(objectsArray, uiGroup, objectsArray.length);

            break;
        case 10:

            var objectsArray = [redMiss, redPreShot, blueGoal, bluePreShot, redMiss, redPreShot, blueGoal, bluePreShot]
            setAnimation(objectsArray, uiGroup, objectsArray.length);

            break;
    }

}

function afterUserInfo ( userData ) {

    __userData = userData;

    if (!__userData.tokenBalance){
        var background = game.add.image(0,0,'stadium');
        background.scale.setTo(.5,.5);
        var goBack = game.add.text(470, 370,'Go back',{fontSize:'12px',fill:'#fff', fontFamily: 'Adventure'});
        goBack.inputEnabled = true;
        goBack.events.onInputOver.add(function(){
            goBack.setStyle({fill: '#eee', fontSize: '12px'});
        });

        goBack.events.onInputOut.add(function(){
            goBack.setStyle({fill: '#fff', fontSize: '12px'});
        });

        goBack.events.onInputDown.add(function(){
            window.history.back();
        });
    }else{
        var background = game.add.image(0, 0, 'stadium');
        background.scale.setTo(.5,.5);
        var vs = game.add.image(game.width/2 - 159, game.height - 120 - 40, 'versus');
        vs.

        game.add.image(game.width / 2 - 234, game.height / 2 - 104 - 40, 'token_ui');

        playButton = game.add.sprite(460, game.height/2 - 26 - 40, 'play_button');
        playButton.animations.add('disabled', [0]);
        playButton.animations.add('available', [1]);
        playButton.animations.add('clicked', [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]);
        playButton.animations.play('available', 15);
        playButton.inputEnabled = true;

        playButton.events.onInputOver.add(function(){
            playButton.animations.add('hover', [1, 2]);
            playButton.animations.play('hover', 15, false);
        });

        playButton.events.onInputOut.add(function(){
            playButton.animations.add('no-hover', [2, 1]);
            playButton.animations.play('no-hover', 15, false);
        });

        playButton.events.onInputDown.add(function(){
            playButton.play('clicked');
            setTimeout(function(){
                soundMusic.loop = true;
                soundMusic.stop();
                game.add.audio('pop').play();
                game.state.start('gamePlayState');
            },1000);
        });

        style = { font: "14px Adventure", fill: "#fff"};
        var text = game.add.text(480 - 10, 5, 'TOKENS', style);
        text = game.add.text(480 - 10, 20, 'POINTS', style);
        text = game.add.text(535 - 10, 5, __userData.tokenBalance, style);
        text = game.add.text(535 - 10, 20, __userData.pointBalance, style);
        style = { font: "14px Montserrat", fill: "#000"};
        dynamicText = game.add.text(268, 122 - 40, '1', style);

        var sound_on = tokensState.add.image(5, 5, 'sound_on');
        var sound_off = tokensState.add.image(5, 20, 'sound_off');
        if(!game.sound.mute)sound_off.alpha = 0;
        else sound_on.alpha = 0;
        sound_on.inputEnabled = true;
        sound_on.events.onInputDown.add(function(target){
            game.sound.mute = true;
            sound_off.alpha = 1;
            target.alpha = 0;
        }, this);

        sound_off.inputEnabled = true;
        sound_off.events.onInputDown.add(function(target){
            game.sound.mute = false;
            sound_on.alpha = 1;
            target.alpha = 0;
        }, this);

        var bar = game.add.image(game.width / 2 - 192, game.height / 2 - 7 - 40, 'bar');
        dragButton = game.add.sprite(92, game.height / 2 - 8 - 40, 'drag_button');
        dragButton.inputEnabled = true;

        dragButton.input.enableDrag(true);
        dragButton.input.allowVerticalDrag = false;
        dragButton.events.onDragUpdate.add(updateDrag);

        bar.inputEnabled = true;
        bar.events.onInputDown.add(function(){
            dragButton.position.x = game.input.mousePointer.x - 10;
            updateDrag();
        });

        var newbie = game.add.sprite(85,220 - 40,'newbie');
        var rookie = game.add.sprite(172,220 - 40,'rookie');
        var skilled = game.add.sprite(282,219 - 40,'skilled');
        var expert = game.add.sprite(410,220 - 40,'expert');

        newbie.animations.add('static', [0]);
        newbie.animations.play('static', 15, false);
        rookie.animations.add('static', [0]);
        rookie.animations.play('static', 15, false);
        skilled.animations.add('static', [0]);
        skilled.animations.play('static', 15, false);
        expert.animations.add('static', [0]);
        expert.animations.play('static', 15, false);

        newbie.animations.add('moving', [1,2,3,4,5,6,7,8,9]);
        rookie.animations.add('moving', [1,2,3,4,5,6,7,8,9]);
        skilled.animations.add('moving', [1,2,3,4,5,6,7,8,9]);
        expert.animations.add('moving', [1,2,3,4,5,6,7,8,9]);

        newbie.inputEnabled = true;
        rookie.inputEnabled = true;
        skilled.inputEnabled = true;
        expert.inputEnabled = true;

        newbie.events.onInputOver.add(function(){
            newbie.animations.play('moving', 15, false);
        });
        rookie.events.onInputOver.add(function(){
            rookie.animations.play('moving', 15, false);
        });
        skilled.events.onInputOver.add(function(){
            skilled.animations.play('moving', 15, false);
        });
        expert.events.onInputOver.add(function(){
            expert.animations.play('moving', 15, false);
        });

        newbie.events.onInputDown.add(function(){dragButton.position.x = 92;updateDrag();});
        rookie.events.onInputDown.add(function(){dragButton.position.x = 182;updateDrag();});
        skilled.events.onInputDown.add(function(){dragButton.position.x = 292;updateDrag();});
        expert.events.onInputDown.add(function(){dragButton.position.x = 432;updateDrag();});
    }

}