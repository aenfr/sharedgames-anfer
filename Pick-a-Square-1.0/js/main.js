var _userData;
var _gameCoreID = 1020;
var _gameID = getParameterFromURL('gameId');
var _currencyId = getParameterFromURL( 'currencyId' );
var isMuted = false;
var _tokensToPlay = 0;
var __prizesAnim;
var hit, afterPick, startScreenSound, whoosh;

// Init
function init(){
	setupUI()
	setupSounds()
	setupStartScreen()
}

// SETUPUI
function setupUI() {
    $('.layer').css({
	    height: ($('.layer').width() * 650 / 750) + "px", 
        maxHeight: window.innerHeight + "px",
        maxWidth: ($(window).height() * 750 / 650) + "px"
    })

    $('.super-container').width($('.layer').width())

	var prizes = {
	    container: document.getElementById("prizes"),
	    renderer: 'svg',
	    loop: false,
	    autoplay: false,
	    rendererSettings: {
	        progressiveLoad:false
	    },
	    animationData: jsons.prizes
	}
	__prizesAnim = bodymovin.loadAnimation(prizes)

    $('.sound-control').click(function(){
        isMuted = !isMuted
		setSoundSettings()
    })
}

// SETUPSOUNDS
function setupSounds() {
	hit = document.getElementById('hit')
	afterPick = document.getElementById('after_pick')
	startScreenSound = document.getElementById('start_screen_music')
	whoosh = document.getElementById('whoosh')
	gameMusic = document.getElementById('game-music')
}

// STARTSCREEN
function setupStartScreen() {

	var startScreen = {
	    container: document.getElementById("start-screen"),
	    renderer: 'svg',
	    loop: true,
	    autoplay: false,
	    rendererSettings: {
	        progressiveLoad:false
	    },
	    animationData: jsons.startScreen
	}
	var startScreenAnim = bodymovin.loadAnimation(startScreen)
	startScreenAnim.play()

	var startButton = {
	    container: document.getElementById("start-button"),
	    renderer: 'svg',
	    loop: false,
	    autoplay: false,
	    rendererSettings: {
	        progressiveLoad:false
	    },
	    animationData: jsons.startButton
	}
	var startButtonAnim = bodymovin.loadAnimation(startButton)

	$('#start-button').click(function() {
		hit.play()
		$(this).addClass('disabled')
		$('#start-screen').fadeOut()
		startButtonAnim.play()
		checkTokensAvailability()
	})

	$(document).trigger('games:onGetUserInformation', [_gameID, _currencyId]);
	
}

function checkTokensAvailability() {

	if (_userData.tokenBalance > 0){ 
		$('#no-tokens-screen').fadeOut()
		$('#token-screen').css({opacity: 1})
		setupTokenScreen()
	} else {
		$('#no-tokens-screen').css({opacity: 1}) 
	}

	if(_userData.status == 0){
		$('#t-s').text(_userData.message)
		$('#no-server-request').fadeIn()
    }
}

// TOKENSCREEN
function setupTokenScreen() {

	$('#indicators-container').width($('#bar').width())
	
	var playButton = {
	    container: document.getElementById("play-button"),
	    renderer: 'svg',
	    loop: false,
	    autoplay: false,
	    rendererSettings: {
	        progressiveLoad:false
	    },
	    animationData: jsons.playButton
	}
	var playButtonAnim = bodymovin.loadAnimation(playButton)

    jsons.balance.layers[0].t.d.k[0].s.t = ""+_userData.pointBalance
    jsons.balance.layers[1].t.d.k[0].s.t = ""+_userData.tokenBalance

	var balanceContainer = {
	    container: document.getElementById("balance"),
	    renderer: 'svg',
	    loop: false,
	    autoplay: false,
	    rendererSettings: {
	        progressiveLoad:false
	    },

	    animationData: jsons.balance
	}
	var balanceContainerAnim = bodymovin.loadAnimation(balanceContainer)

	$('#play-button').click(function() {
		startScreenSound.pause()
		gameMusic.play()
		gameMusic.loop = true
		hit.play()	
		playButtonAnim.play()
		$(this).addClass('disabled')
        _tokensToPlay = parseInt($('#tokens-amount').text())
		$( document ).trigger ( 'games:onPlayGame', [ _gameID, _currencyId, _tokensToPlay ] );
		
	})

    $('.draggable').draggable({ 
        axis: "x",
        containment: "#bar",
        start: function() {
            updateDraggableValue($('.draggable').css('left'))
        },
        drag: function() {
            updateDraggableValue($('.draggable').css('left'))
        },
        stop: function() {
            updateDraggableValue($('.draggable').css('left'))
        }
    });

    $('#bar').click(function(e){
        var distanceA = ($('body').width()/2) - ($('.layer').width()/2)
        var distanceB = ($('.layer').width()/2) - ($('#token-ui').width()/2)
        var distanceC = ($('#token-ui').width()/2) - ($('#bar').width()/2)
        var value = e.clientX - (distanceA + distanceB + distanceC) - ($('.draggable').width()/2)
        $('.draggable').css({left: value})
        updateDraggableValue($('.draggable').css('left'))
    })
	
}

// SetupGameplayScreen
function setupGameplayScreen() {

	var prizeButton = {
	    container: document.getElementById("prize-button"),
	    renderer: 'svg',
	    loop: false,
	    autoplay: false,
	    rendererSettings: {
	        progressiveLoad:false
	    },
	    animationData: jsons.prizesButton
	}
	var prizeButtonAnim = bodymovin.loadAnimation(prizeButton)

	$('#prize-button').click(function() {
		$(this).addClass('disabled')
		prizeButtonAnim.play()		
		$('#prizes').css({zIndex: 102})
		__prizesAnim.playSegments([0, 11], true)
		$('#prizes').click(function() {
			__prizesAnim.playSegments([12, 29], true)
			setTimeout(function(){
				$('#prizes').css({zIndex: -1})	
			}, 500)
		})
	})

	var playID = __tokensResponse.playId
	jsons.main.assets[1].layers[0].ef[playID - 1].ef[0].v.k = 1

    jsons.main.layers[42].t.d.k[0].s.t = ""+_userData.pointBalance
    jsons.main.layers[43].t.d.k[0].s.t = ""+_userData.tokenBalance

    jsons.main.layers[0].t.d.k[0].s.t = ""+_tokensToPlay
    jsons.main.layers[1].t.d.k[0].s.t = ""+__tokensResponse.totalPoints

	var gameplay = {
	    container: document.getElementById("gameplay"),
	    renderer: 'svg',
	    loop: true,
	    autoplay: false,
	    rendererSettings: {
	        progressiveLoad:false
	    },
	    animationData: jsons.main
	}
	var gameplayAnim = bodymovin.loadAnimation(gameplay)
	
	$('#token-screen').fadeOut()
	$('#loader-screen').fadeOut()
	gameplayAnim.playSegments([0, 59], true)
	
	setTimeout(function(){
		
		appendFakeCubes()	
		
		$('.supercube').click(function() {
			
			$(this).addClass('disabled')
			$('.supercube').addClass('disabled')

			gameMusic.pause()
			afterPick.play()
			
			$('#prize-button').fadeOut()

			let selectedIndex = $(this).attr('id').split('-')[1] - 1
			jsons.main.layers[5].ef[selectedIndex].ef[0].v.k[0].s[0] = 0
			jsons.main.layers[5].ef[selectedIndex].ef[0].v.k[1].s[0] = 1
			gameplayAnim.loop = false
			gameplayAnim.playSegments([60, 375], true)

			var playAgainButton = {
			    container: document.getElementById("play-again"),
			    renderer: 'svg',
			    loop: false,
			    autoplay: false,
			    rendererSettings: {
			        progressiveLoad:false
			    },
			    animationData: jsons.playAgainButton
			}
			var playAgainButtonAnim = bodymovin.loadAnimation(playAgainButton)
			
			$('#play-again').click(function() {
				playAgainButtonAnim.play()	
		        hit.play()
		        setTimeout(function(){
		        	var mainURL = location.protocol + '//' + location.host + location.pathname
		            window.location.href = mainURL+ window.location.search+"&no-init=true&isMuted=" + isMuted
		        },500)
			})

			setTimeout(function() {
				$('#play-again-container').css({opacity: 1})
				$(document).trigger('games:onUpdateBalances');
			}, 6500)
			
		})		
	}, 1000)

}

function updateDraggableValue(val){
    var value = parseInt(val)
    var barWidth = $('#bar').width() - $('.draggable').width()
    var newVal = value * 250 / barWidth
    
    if(newVal > 250){ 
        newVal = 250
    }

    if(newVal < 1) newVal = 1
    if(newVal > _userData.tokenBalance) newVal = _userData.tokenBalance
    $('#tokens-amount').text(parseInt(newVal))
}

function appendFakeCubes() {
	for(var i = 1; i <= 9; i++) {
		var cubePosition = $('.cube#cube-' + i).position()
		var divString = "<div class='supercube' id='supercube-" + i + "'></div>"
		var leftMargin = $('.main-container').position().left
		
		$('#game-screen').append(divString)
		$('#supercube-' + i).css({
			position: 'absolute',
			cursor: 'pointer',
			left: ((cubePosition.left - leftMargin) + 6) + "px",
			top: (cubePosition.top + 12) + "px",
			width: '10%',
			height: '12%',
			zIndex: 103
		})
	}	
}

function setSoundSettings() {
    if(!isMuted) {$('.sound-control').attr('src','./assets/img/sound_on.png')}
    else {$('.sound-control').attr('src','./assets/img/sound_off.png')}

	hit.volume = (isMuted) ? 0 : 1
	afterPick.volume = (isMuted) ? 0 : 1
	startScreenSound.volume = (isMuted) ? 0 : 1
	whoosh.volume = (isMuted) ? 0 : 1
	gameMusic.volume = (isMuted) ? 0 : 1
}

function afterUserInfo ( userData ) {

	_userData = userData
	$('#start-button').removeClass('disabled')
	$('#loader-screen').fadeOut()

	if(window.location.href.match("no-init") != null){
		isMuted = (getParameterFromURL('isMuted') == 'true');
		setSoundSettings()
		$('#start-button').trigger('click')
	} else {
		startScreenSound.loop = true
		startScreenSound.play()
	}

}

function afterPlay ( data ) {

	$('#loader-screen').fadeIn()
	__tokensResponse = data
	setupGameplayScreen();

}