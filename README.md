# Snowfly Games #

Welcome, this is the repository that Snowfly will add the games to for SQM. 
The basis of the games is fairly simple. Each game resides in its own directory, 
and is built to be stand-alone. It will pull the necessary resource files 
in on the index file and all the assets are included in the directory.

### Instructions ###

This is fairly easy to set-up. Setup your communication between the game and your backend controller in the root directory 
in the clientLogic.js. Ensure that you keep the clientLogic.js un-tracked. You will find three basic methods in that 
file that the game will use to establish the data that it needs. If more are needed feel free to use that file to build 
out the additional functions you need.

To ensure that the clientLogic.js files changes are not tracked within this repository please run the following:

    git update-index --assume-unchanged clientLogic.js
    
For more info: [ignoring versioned files](https://help.github.com/en/github/using-git/ignoring-files#ignoring-versioned-files)


### Contribution ###

* Pull requests may be submitted for any changes you wish to apply.

### Who do I talk to? ###

* Contact [kmurphy@snowfly.com](kmurphy@snowfly.com)
* Contact [support@snowfly.com](support@snowfly.com)
* Submit [asana](https://asana.com) task in shared project

### Additional Information ###

This readme will grow as we continue to develop and contribute to contents. Good luck!