// Global variables
//Game Core Id: 1002
var __gameId = getParameterFromURL ( 'gameId' );//1002;
var _currencyId = getParameterFromURL ( 'currencyId' );
var s3Path = 'https://s3-us-west-2.amazonaws.com/snowflyeverest/media/gameResources/soak-a-mole';
var __userData = {tokenBalance: 0, pointBalance: 0},
    __tokensToPlayJSON,
    __tokensToPlay,
    __molesHit,
    __gamePoints = 0,
    __tenTimeOut = 1,
    __fiveTimeOut = 1,
    backgroundSound,
    bonusSound;
var molePositions = {x: [20,260,138,374,222,36,374], y: [35,30,82,84,144,178,188]};
var game = new Phaser.Game(550, 400, Phaser.CANVAS, 'Mole');
//Preload 'loading' images

var bootState = {

    preload: function(){
        $( document ).trigger ( 'games:onGetUserInformation', [ __gameId, _currencyId ] );
    },
    create: function(){
        //moving to the loaderState
        game.state.start('loaderState');

    }

};

var loaderState = {

    preload: function(){

        //showing loaging images
        this.stage.backgroundColor = "#fff";
        //var loading = game.add.sprite(0, 0, 'loading');
        //loading.animations.add('loading');
        //loading.animations.play('loading', 9, true);


        //loading static images
        this.load.image('bg_crop', s3Path+'/assets/img/bg_crop.png');
        this.load.image('bonusTitle', s3Path+'/assets/img/bonusTitle.png');
        this.load.image('congrats', s3Path+'/assets/img/congrats.png');
        this.load.image('hammer', s3Path+'/assets/img/hammer.png');
        this.load.image('popup_1', s3Path+'/assets/img/popup_1.png');
        this.load.image('popup_2', s3Path+'/assets/img/popup_2.png');
        this.load.image('scoreUpdate', s3Path+'/assets/img/scoreUpdate.png');
        this.load.image('soak_a_mole', s3Path+'/assets/img/soak_a_mole.png');
        this.load.image('start_bg', s3Path+'/assets/img/start_bg.png');
        this.load.image('token_point_bg', s3Path+'/assets/img/token_point_bg.png');
        this.load.image('layer', s3Path+'/assets/img/layer.png');
        this.load.image('sound_on', s3Path+'/assets/img/sound_on.png');
        this.load.image('sound_off', s3Path+'/assets/img/sound_off.png');

        //Loading sprites
        this.load.spritesheet('1_go', s3Path+'/assets/sprites/1_go.png', 227, 227, 38);
        this.load.spritesheet('2', s3Path+'/assets/sprites/2.png', 227, 227, 18);
        this.load.spritesheet('3', s3Path+'/assets/sprites/3.png', 227, 227, 18);
        this.load.spritesheet('arrow_pop_up', s3Path+'/assets/sprites/arrow_pop_up.png', 77, 60, 34);
        this.load.spritesheet('betBg', s3Path+'/assets/sprites/betBg.png', 101, 47, 3);
        this.load.spritesheet('bonusCharIn', s3Path+'/assets/sprites/bonusCharIn.png', 211, 177, 30);
        this.load.spritesheet('bonusCharMoveAni', s3Path+'/assets/sprites/bonusCharMoveAni.png', 211, 177, 36);
        this.load.spritesheet('bonusWaterAnimation', s3Path+'/assets/sprites/bonusWaterAnimation.png', 151, 177, 22);
        this.load.spritesheet('homepage', s3Path+'/assets/sprites/homepage.png', 10, 10, 1);
        this.load.spritesheet('mole1', s3Path+'/assets/sprites/mole1.png', 143, 143, 58);
        this.load.spritesheet('playAgain', s3Path+'/assets/sprites/playAgain.png', 121, 38, 2);
        this.load.spritesheet('skipToBonus', s3Path+'/assets/sprites/skipToBonus.png', 318, 58, 3);
        this.load.spritesheet('start_mole_animation', s3Path+'/assets/sprites/start_mole_animation.png', 140, 140, 35);
        this.load.spritesheet('start', s3Path+'/assets/sprites/start.png', 107, 102, 3);
        this.load.spritesheet('timer_5', s3Path+'/assets/sprites/timer_5.png', 42, 43, 6);
        this.load.spritesheet('timer', s3Path+'/assets/sprites/timer.png', 44, 44, 11);
        this.load.spritesheet('token_animation', s3Path+'/assets/sprites/token_animation.png', 10, 10, 1);
        this.load.spritesheet('tokens', s3Path+'/assets/sprites/tokens.png', 60, 123, 18);
        this.load.spritesheet('winReward', s3Path+'/assets/sprites/winReward.png', 512, 74, 2);

        //Loading sounds
        this.load.audio('background', s3Path+'/assets/sounds/background.mp3');
        this.load.audio('bonusScreen', s3Path+'/assets/sounds/bonusScreen.mp3');
        this.load.audio('hitWithSplash', s3Path+'/assets/sounds/hitWithSplash.mp3');
        this.load.audio('moleUp', s3Path+'/assets/sounds/moleUp.mp3');
        this.load.audio('popup', s3Path+'/assets/sounds/popup.mp3');
        this.load.audio('timer', s3Path+'/assets/sounds/timer.mp3');
        this.load.audio('waterSplash', s3Path+'/assets/sounds/waterSplash.mp3');


    },
    create: function(){

        //moving to the first gameplay state
        game.state.start('initState');

    },
}

var initState = {

    tokensAssetdata: {
        position: {
            x: [18, 68, 118, 168, 218], 
            y: [245, 245, 245, 245, 245]
        },
        spritesRange: {
            over: [1, 4, 7, 10, 13],
            static: [0, 3, 6, 9, 12]
        },
        values: [1, 5, 10, 25, 50]
    },

    create: function(){

        $( document ).trigger ( 'games:onGetUserInformation', [ __gameId, _currencyId ] );

        __tokensToPlay = 0;
        __gamePoints = 0;
        __molesHit = 0;
        __tenTimeOut = 1;

        backgroundSound = game.add.audio('background');
        backgroundSound.play();

        game.add.image(0, 0 , 'start_bg');
        game.add.image(10, 10, 'token_point_bg');
        game.add.image(155, 10, 'soak_a_mole');

        var popUp1 = game.add.image(-20, 165, 'popup_1');
        var arrow = game.add.sprite(133, 260, 'arrow_pop_up');
        arrow.animations.add('vertialFloatig', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0]);
        arrow.animations.add('horizontalFloating',[18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,31,30,29,28,27,26,25,24,23,22,21,20,19,18]);
        arrow.animations.play('vertialFloatig', 20, true);

        var sound_on = this.add.image(5, 55, 'sound_on');
        var sound_off = this.add.image(5, 70, 'sound_off');
        if(!game.sound.mute)sound_off.alpha = 0;
        else sound_on.alpha = 0;
        sound_on.inputEnabled = true;
        sound_on.events.onInputDown.add(function(target){
            game.sound.mute = true;
            sound_off.alpha = 1;
            target.alpha = 0;
        }, this);

        sound_off.inputEnabled = true;
        sound_off.events.onInputDown.add(function(target){
            game.sound.mute = false;
            sound_on.alpha = 1;
            target.alpha = 0;
        }, this);

        game.add.sprite(284, 325, 'betBg');
        var betBg = game.add.sprite(285, 325, 'betBg');
        betBg.animations.add('button', [1]);
        betBg.animations.add('over', [2]);
        betBg.animations.play('button');

        var style = { font: "18px Montserrat", fill: "#fff"};
        var text = game.add.text(308, 334, __tokensToPlay, style);
        game.add.text(80, 10, __userData.tokenBalance, style);
        game.add.text(80, 32, __userData.pointBalance, style);

        betBg.inputEnabled = true;
        betBg.events.onInputOver.add(function(){if(__tokensToPlay)betBg.animations.play('over', 20, false);});
        betBg.events.onInputOut.add(function(){betBg.animations.play('button', 20, false);});
        betBg.events.onInputDown.add(function(){
            __tokensToPlay = 0;
            text.destroy();
            text = game.add.text(308, 334, __tokensToPlay.toString(), style);
        });

        game.add.sprite(400, 270, 'start');
        var startSign = game.add.sprite(400, 270, 'start');
        startSign.animations.add('sign', [1]);
        startSign.animations.add('over', [2]);
        startSign.animations.play('sign');

        startSign.inputEnabled = true;
        startSign.events.onInputOver.add(function(){if(__tokensToPlay)startSign.animations.play('over', 20, false);});
        startSign.events.onInputOut.add(function(){startSign.animations.play('sign', 20, false);});
        startSign.events.onInputDown.add(function(){
            if(__tokensToPlay) game.state.start('gameState');
        });

        var startAnimation = game.add.sprite(90, 34, 'start_mole_animation');
        startAnimation.animations.add('moving', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35]);
        startAnimation.play('moving', 20, false);
        setInterval(function(){
            startAnimation.play('moving', 20, false);
        },4000);

        var popUp2 = game.add.image(5, 165, 'popup_2');
        popUp2.alpha = 0;

        var tokens = [];
        for(var i = 0; i < initState.tokensAssetdata.position.x.length; i++){
            tokens[i] = game.add.sprite(initState.tokensAssetdata.position.x[i], initState.tokensAssetdata.position.y[i], 'tokens');
            tokens[i].animations.add('static', [initState.tokensAssetdata.spritesRange.static[i]]);
            tokens[i].animations.add('over', [initState.tokensAssetdata.spritesRange.over[i]]);
            tokens[i].animations.play('static', 20, false);
            tokens[i].inputEnabled = true;
            tokens[i].property = i;
            tokens[i].events.onInputDown.add(function(e){
                var value = initState.tokensAssetdata.values[e.property];
                __tokensToPlay = (__tokensToPlay + value) > __userData.tokenBalance ? __userData.tokenBalance : (value + __tokensToPlay) > 250 ? 250 : (value + __tokensToPlay);  
                text.destroy();
                text = game.add.text(308, 334, __tokensToPlay.toString(), style);
                popUp1.alpha = 0;
                popUp2.alpha = 1;
                arrow.x = 323;
                arrow.y = 290;
                arrow.animations.play('horizontalFloating', 20, true);
            });
        }

        tokens[0].events.onInputOver.add(function(){tokens[0].animations.play('over', 20, false);});
        tokens[0].events.onInputOut.add(function(){tokens[0].animations.play('static', 20, false);});
        tokens[1].events.onInputOver.add(function(){tokens[1].animations.play('over', 20, false);});
        tokens[1].events.onInputOut.add(function(){tokens[1].animations.play('static', 20, false);});
        tokens[2].events.onInputOver.add(function(){tokens[2].animations.play('over', 20, false);});
        tokens[2].events.onInputOut.add(function(){tokens[2].animations.play('static', 20, false);});
        tokens[3].events.onInputOver.add(function(){tokens[3].animations.play('over', 20, false);});
        tokens[3].events.onInputOut.add(function(){tokens[3].animations.play('static', 20, false);});
        tokens[4].events.onInputOver.add(function(){tokens[4].animations.play('over', 20, false);});
        tokens[4].events.onInputOut.add(function(){tokens[4].animations.play('static', 20, false);});

    }

};

var gameState = {

    prizesTable: [0, 2, 4, 8, 10, 25, 100, 250, 500, 2500, 5000],

    create: function(){
        $( document ).trigger ( 'games:onPlayGame', [ __gameId, _currencyId, __tokensToPlay ] );

        game.add.image(0, 0, 'bg_crop');
        var countingSound = game.add.audio('timer');
        var interval;

        var sound_on = this.add.image(5, 55, 'sound_on');
        var sound_off = this.add.image(5, 70, 'sound_off');
        if(!game.sound.mute)sound_off.alpha = 0;
        else sound_on.alpha = 0;
        sound_on.inputEnabled = true;
        sound_on.events.onInputDown.add(function(target){
            game.sound.mute = true;
            sound_off.alpha = 1;
            target.alpha = 0;
        }, this);

        sound_off.inputEnabled = true;
        sound_off.events.onInputDown.add(function(target){
            game.sound.mute = false;
            sound_on.alpha = 1;
            target.alpha = 0;
        }, this);

        var tree = game.add.sprite(game.width/2 - 113, game.height/2 - 113, '3');
        tree.animations.add('counting');
        tree.animations.play('counting', 25, false);
        tree.events.onAnimationComplete.add(function(){
            tree.alpha = 0;
            var two = game.add.sprite(game.width/2 - 113, game.height/2 - 113, '2');
            two.animations.add('counting');
            two.animations.play('counting', 25, false);
            two.events.onAnimationComplete.add(function(){
                two.alpha = 0;
                var one = game.add.sprite(game.width/2 - 113, game.height/2 - 113, '1_go');
                one.animations.add('counting');
                one.animations.play('counting', 25, false); 
                one.events.onAnimationComplete.add(function(){
                    $('canvas').mousemove(function(e){
                        $('.ballon').css({
                            display: 'block',
                            top: e.clientY + 10,
                            left: e.clientX + 10,
                        });
                        $('canvas').css({
                            cursor: 'none'
                        });
                    });
                    one.alpha = 0;
                    outingMole();
                    var tenTimer = game.add.sprite(490, 2, 'timer');
                    tenTimer.animations.add('running', [0,1,2,3,4,5,6,7,8,9]);
                    tenTimer.animations.add('stoped', [10]);
                    tenTimer.animations.play('running', 1, false);
                    interval = setInterval(function(){
                        countingSound.play();
                    },1000);
                    tenTimer.events.onAnimationComplete.add(function(){
                        clearInterval(interval);
                        __tenTimeOut = 0;
                        tenTimer.play('stoped', 20, false);
                        tenTimer.destroy();
                        //skipToBonus.animations.play('over', 10, true);
                        game.add.image(0, 0, 'layer');
                        game.add.image(50, 120, 'scoreUpdate');
                        var style = { font: "18px Montserrat", fill: "#fff"};
                        setTimeout(function(){
                            game.add.text(game.width / 2, 158, __molesHit, style);
                            game.add.text(game.width / 2, 208   , __tokensToPlayJSON.totalPoints, style);
                            $(document).trigger('games:onUpdateBalances');
                        },200);
                        $('canvas')
                            .off('mousemove')
                            .css({cursor: 'auto'});
                        $('.ballon').css({display: 'none'});

                        var playAgain = game.add.sprite(game.width/2 - 60, 300, 'playAgain');
                        playAgain.animations.add('static', [0]);
                        playAgain.animations.add('over', [1]);
                        playAgain.animations.play('static', 1, false);
                        playAgain.inputEnabled = true;
                        playAgain.events.onInputOver.add(function(){
                            playAgain.animations.play('over');
                        });
                        playAgain.events.onInputOut.add(function(){
                            playAgain.animations.play('static');
                        });
                        playAgain.events.onInputDown.add(function(){
                            game.state.start('initState');
                            // window.top.updateBalances();
                        });
                    });
                });
            });
        });

    }
   
};

var mole = [];
var moleIterator = 0;
var scoreText;

function outingMole(){
    var style = { font: "18px Montserrat", fill: "#fff"};
    var n = Math.floor((Math.random() * molePositions.x.length));
    var points = Math.floor((Math.random() * 5) + 1);

    mole[moleIterator] = game.add.sprite(molePositions.x[n], molePositions.y[n], 'mole1');
    mole[moleIterator].animations.add('outing', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,20,22,24,26,28,30]);
    mole[moleIterator].animations.add('splashed', [31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58]);
    mole[moleIterator].animations.play('outing',25, false);
    game.add.audio('moleUp').play();

    mole[moleIterator].inputEnabled = true;
    mole[moleIterator].events.onInputDown.add(function(){
        mole[moleIterator].animations.play('splashed', 30, false);
        game.add.audio('waterSplash').play();
        __gamePoints += points;
        try {
            scoreText.destroy();
        } catch (e) {

        }
        scoreText = game.add.text(120, 12, __gamePoints, style );
        __molesHit++;
    });

    mole[moleIterator].events.onAnimationComplete.add(function(){
        mole[moleIterator].destroy();
        moleIterator++;
        if(__tenTimeOut) outingMole();
    });
};


//setting all the game-states
game.state.add('loaderState', loaderState);
game.state.add('bootState', bootState);
game.state.add('initState', initState);
game.state.add('gameState', gameState);
game.state.start('bootState');

function afterUserInfo ( userData ) {

    __userData = userData;

    bootState.stage.disableVisibilityChange = true;
    bootState.load.spritesheet('loading', s3Path+'/assets/sprites/loading.png', 135.9, 138.2, 38);
    game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

    // Have the game centered horizontally
    game.scale.pageAlignHorizontally = true;

    // And vertically
    game.scale.pageAlignVertically = true;

}

function afterPlay ( data ) {

    __tokensToPlayJSON = data;

    __playId = __tokensToPlayJSON.playId;

}