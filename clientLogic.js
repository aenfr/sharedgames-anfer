function getUserInformation ( gameId, currencyId ) {

    //EXAMPLE
    // the following variables are some testing variables. That we will use to test the game play flow.
    gameId = 999;
    currencyId = 1024;

    let tempdata = null;

    // I am returning am returning this ajax call so that I can use a .done call, you can return a promise which will
    // serve the same purpose.
    return $.ajax({
        type: "POST",
        url: "http://localhost/gameapi/v1/getStartInfo",
        data: { gameId: gameId, currencyId: currencyId },
        success: function( data ) {
            tempdata = data;
        }
    } );

}

function postGamePlay ( gameId, currencyId, amount ) {

    //EXAMPLE
    gameId = 999;
    currencyId = 1024;

    let tempdata = null;

    // I am returning am returning this ajax call so that I can use a .done call, you can return a promise which will
    // serve the same purpose.
    return $.ajax ( {
        type: "POST",
        url: "http://localhost/gameapi/v1/playGame",
        data: { tokens: amount, gameId: gameId, currencyId: currencyId },
        success: function ( data ) {
            tempdata = data;
        }
    } );

}

function updateUserBalance ( ) {

    // This is just an example of what you could do, and will vary upon your implementation.
    $.ajax ( {
        type: "GET",
        url: "http://localhost/gameapi/v1/getUserBalance",
    } ).done ( function ( newBalance ) {
        $( '#user-balance-area' ).text( newBalance );
    } );

}
