//Global variables
var _eggJSON
var _lap = 1;
var _userData;
var _router = 1;
var _gameCoreID = 1015;
var _gameID = getParameterFromURL('gameId');
var _currencyId = getParameterFromURL( 'currencyId' );
var _tokensToPlay;
var _eggType = 0;
var _isMuted = false;
var __tokensResponse;
var _toPlayAnim
var isMuted = false
var $xp = 0,
    $yp = 0;
var $mouseX = 0,
    $mouseY = 0;
var soundtrack,
    hit,
    breakSound,
    whoosh,
    prize,
    crowd,
    dinoSound;
var color = {
    light: '', dark: '', darker: '', base: '', eyeBase: '', eyeLight: '', eggDot: ''
}

function init() {

	setupLayerClass()
	
	flexibility(document.documentElement);
	
	setupSounds()
    getUserData()

    $('.about').click(function(){
        $('#instruction').css({zIndex: 102})
    })

    $('.instructions-image').click(function(){
        $('#instruction').css({zIndex: -1})
    })
}

function setupGameplay() {
    
    dinoSound.play()
    whoosh.play()
    var ui = {
        container: document.getElementById("gameplay-ui"),
        renderer: 'svg',
        loop: true,
        autoplay: false,
        rendererSettings: {
            progressiveLoad:false
        },

        animationData: jsons.UI
    }

    var uiAnim = bodymovin.loadAnimation(ui)
    uiAnim.play()

    var staticEgg = ""
    if (_tokensToPlay < 10) { 
        _eggJSON = jsons.eggs.regular
        _eggType = 0
    }
    else if (_tokensToPlay >= 10 && _tokensToPlay < 50) { 
        _eggJSON = jsons.eggs.good 
        _eggType = 1
    }
    else if (_tokensToPlay >= 50 && _tokensToPlay < 250) {   
        _eggJSON = jsons.eggs.special 
        _eggType = 2
    }
	else if (_tokensToPlay == 250) { 
        _eggJSON = jsons.eggs.magic 
        _eggType = 3
    }

    if (__tokensResponse.playId == 10 ){
        var randomArray = [1, 1, 1]
    } else {
        var randomArray = [0, 0, 1].sort(function() {
          return .5 - Math.random();
        });        
    }

    var dinnoIndex = getDinnoByID(__tokensResponse.playId)

    var column1 = {
        container: document.getElementById("column-1"),
        renderer: 'svg',
        loop: false,
        autoplay: false,
        rendererSettings: {
            progressiveLoad:false
        },

        animationData: randomArray[0] == 1 ? _eggJSON : jsons.losingEgg.pop()
    }

    setupEggs(randomArray, column1, dinnoIndex, 0, __tokensResponse.playId) 
    var column1Anim = bodymovin.loadAnimation(column1)

    var column2 = {
        container: document.getElementById("column-2"),
        renderer: 'svg',
        loop: false,
        autoplay: false,
        rendererSettings: {
            progressiveLoad:false
        },

        animationData: randomArray[1] == 1 ? _eggJSON : jsons.losingEgg.pop()
    }

    setupEggs(randomArray, column2, dinnoIndex, 1, __tokensResponse.playId )
    var column2Anim = bodymovin.loadAnimation(column2)

    var column3 = {
        container: document.getElementById("column-3"),
        renderer: 'svg',
        loop: false,
        autoplay: false,
        rendererSettings: {
            progressiveLoad:false
        },

        animationData: randomArray[2] == 1 ? _eggJSON : jsons.losingEgg.pop()
    }

    setupEggs(randomArray, column3, dinnoIndex, 2, __tokensResponse.playId )
    var column3Anim = bodymovin.loadAnimation(column3)

    var animationsArray = [
	    column1Anim,
	    column2Anim,
	    column3Anim
    ]

	$('.egg-column').click(function() {
        fillDino(color)
        breakSound.play()
        dinoSound.play()
        whoosh.play()
        var index = $('.egg-column').index(this)
        
        if (__tokensResponse.playId == 10) {
            animationsArray[0].play()
            animationsArray[1].play()
            animationsArray[2].play()
        } else {
            animationsArray[index].play()
        }
        

        if (randomArray[index] == 1) {
            setTimeout(function(){
                triggerFinalAnimation()
            }, 3000)
        }
	})
}

function setupEggs(randomArray, animation, dinnoIndex, index, playID){

    if (randomArray[index] == 1) {
        if (playID == 10){
            animation.animationData.layers[0].ef[0].ef[0].v.k = 0
            animation.animationData.layers[0].ef[1].ef[0].v.k = 0
            animation.animationData.layers[0].ef[2].ef[0].v.k = 0
            animation.animationData.layers[0].ef[index].ef[0].v.k = 1
        } else {
            animation.animationData.layers[0].ef[dinnoIndex].ef[0].v.k = 1        
        }   
    } else {
        animation.animationData.layers[0].ef[_eggType].ef[0].v.k = 1 
        if (_eggType == 0 || _eggType == 1) {
            if (Math.round(Math.random()) == 0) {
                animation.animationData.layers[1].ef[0].ef[0].v.k = 1
                animation.animationData.layers[1].ef[1].ef[0].v.k = 0
            } else {
                animation.animationData.layers[1].ef[0].ef[0].v.k = 0
                animation.animationData.layers[1].ef[1].ef[0].v.k = 1
            }            
        }
    }
}

function triggerFinalAnimation(){
    
    soundtrack.pause()
    
    setTimeout(function(){
        prize.play()
        $(document).trigger('games:onUpdateBalances');
    }, 1000)
    
    whoosh.play()

    if (__tokensResponse.playId == 10) {
        crowd.play()
    }
    
    $('body').css({cursor: 'default'})
    $('#hammer').remove()

    $('#winning-screen').css({zIndex: 101})

    $('#winning-screen').click(function() {
        hit.play()
        setTimeout(function(){
            var mainURL = location.protocol + '//' + location.host + location.pathname
            window.location.href = mainURL+ window.location.search+"&no-init=true&isMuted=" + isMuted
        },500)
    })

   var winning = {
        container: document.getElementById("winning-screen"),
        renderer: 'svg',
        loop: false,
        autoplay: false,
        rendererSettings: {
            progressiveLoad:false
        },
        animationData: jsons.winning
    }

    winning.animationData.layers[0].ef[3].ef[0].v.k = 0
    winning.animationData.layers[0].ef[_eggType].ef[0].v.k = 1
    winning.animationData.layers[6].ef[__tokensResponse.playId - 1].ef[0].v.k = 1
    winning.animationData.layers[4].t.d.k[0].s.t = _tokensToPlay+""
    winning.animationData.layers[5].t.d.k[0].s.t = __tokensResponse.totalPoints+""
    
    var winningAnim = bodymovin.loadAnimation(winning)
    winningAnim.play()

}

function getDinnoByID(playID) {
	switch(playID){
		case 1: case 4: case 7:
			return 0
		case 2: case 5: case 8:
			return 1
		case 3: case 6: case 9:
			return 2
	}
	return 0
}


function setupLayerClass() {

    if(detectIE()) {
        $('.button-container').width($('.egg-token-column').width())    
        $('.button-container').removeClass('w-100')
    }

    $('.layer, .super-container').css({
	    height: ($('.layer').width() * 650 / 750) + "px", 
        maxHeight: window.innerHeight + "px",
        maxWidth: ($(window).height() * 750 / 650) + "px"
    })

    $('.buttons-container').width($('.layer').width())
}

function getUserData() {

    $( document ).trigger ( 'games:onGetUserInformation', [ _gameID, _currencyId ] );

}


function setupSounds() {
    
    dinoSound = document.getElementById('dinosound')
    soundtrack = document.getElementById('soundtrack')
    hit = document.getElementById('hit');
    whoosh = document.getElementById('whoosh');
    prize = document.getElementById('prize');
    breakSound = document.getElementById('break');
    crowd = document.getElementById('crowd');

   $('.sound-control').click(function(){
	    isMuted = !isMuted
        setSoundSettings()
	})
}

function setupStartScreen() {

    if(window.location.href.match("no-init") != null){
        setupTokensScreen()
        isMuted = (getParameterFromURL('isMuted') == 'true');
        setSoundSettings()
        $('#start-screen').fadeOut()
    } else {
        soundtrack.play();
        soundtrack.loop = true
    }

    var startButton = {
        container: document.getElementById("start-button"),
        renderer: 'svg',
        loop: false,
        autoplay: false,
        rendererSettings: {
            progressiveLoad:false
        },

        animationData: jsons.startButton
    }
    var startButtonAnim = bodymovin.loadAnimation(startButton)

    $('#start-button').click(function(){
        $(this).addClass('disabled')
    	hit.play()
		startButtonAnim.play()    	
		startButtonAnim.addEventListener("complete", function() {
			setupTokensScreen()
			$('#start-screen').fadeOut()
		})
    })
}

function updateTokensToPlay(value) {
    $('#to-play').html("")
    jsons.toPlay.layers[0].t.d.k[0].s.t = ""+value
    var toPlay = {
        container: document.getElementById("to-play"),
        renderer: 'svg',
        loop: false,
        autoplay: false,
        rendererSettings: {
            progressiveLoad:false
        },
        animationData: jsons.toPlay
    }

    _toPlayAnim = bodymovin.loadAnimation(toPlay)
}

function setupTokensScreen() {

    updateTokensToPlay("0")

    var overlay = {
        container: document.getElementById("overlay"),
        renderer: 'svg',
        loop: false,
        autoplay: false,
        rendererSettings: {
            progressiveLoad:false
        },
        animationData: jsons.overlay
    }

    var overlayAnim = bodymovin.loadAnimation(overlay)
        
    var egg1 = {
        container: document.getElementById("egg-1"),
        renderer: 'svg',
        loop: false,
        autoplay: false,
        rendererSettings: {
            progressiveLoad:false
        },
        animationData: jsons.tokenEggs.egg1
    }

    var egg1Anim = bodymovin.loadAnimation(egg1)

    var egg10 = {
        container: document.getElementById("egg-10"),
        renderer: 'svg',
        loop: false,
        autoplay: false,
        rendererSettings: {
            progressiveLoad:false
        },
        animationData: jsons.tokenEggs.egg10
    }

    var egg10Anim = bodymovin.loadAnimation(egg10)

    var egg50 = {
        container: document.getElementById("egg-50"),
        renderer: 'svg',
        loop: false,
        autoplay: false,
        rendererSettings: {
            progressiveLoad:false
        },
        animationData: jsons.tokenEggs.egg50
    }

    var egg50Anim = bodymovin.loadAnimation(egg50)

    var egg250 = {
        container: document.getElementById("egg-250"),
        renderer: 'svg',
        loop: false,
        autoplay: false,
        rendererSettings: {
            progressiveLoad:false
        },
        animationData: jsons.tokenEggs.egg250
    }

    var egg250Anim = bodymovin.loadAnimation(egg250)

    var button1 = {
        container: document.getElementById("button-1"),
        renderer: 'svg',
        loop: false,
        autoplay: false,
        rendererSettings: {
            progressiveLoad:false
        },
        animationData: jsons.tokenButtons.button1
    }

    var button1Anim = bodymovin.loadAnimation(button1)

    var button10 = {
        container: document.getElementById("button-10"),
        renderer: 'svg',
        loop: false,
        autoplay: false,
        rendererSettings: {
            progressiveLoad:false
        },
        animationData: jsons.tokenButtons.button10
    }

    var button10Anim = bodymovin.loadAnimation(button10)

    var button50 = {
        container: document.getElementById("button-50"),
        renderer: 'svg',
        loop: false,
        autoplay: false,
        rendererSettings: {
            progressiveLoad:false
        },
        animationData: jsons.tokenButtons.button50
    }

    var button50Anim = bodymovin.loadAnimation(button50)

    var button250 = {
        container: document.getElementById("button-250"),
        renderer: 'svg',
        loop: false,
        autoplay: false,
        rendererSettings: {
            progressiveLoad:false
        },
        animationData: jsons.tokenButtons.button250
    }

    var button250Anim = bodymovin.loadAnimation(button250)

    var playButton = {
        container: document.getElementById("play-button"),
        renderer: 'svg',
        loop: false,
        autoplay: false,
        rendererSettings: {
            progressiveLoad:false
        },
        animationData: jsons.playButton
    }

    var playButtonAnim = bodymovin.loadAnimation(playButton)

    if(window.location.href.match("no-init") == null){
        isMuted = (getParameterFromURL('isMuted') == 'true');
        setSoundSettings()
        var guide = {
            container: document.getElementById("guide"),
            renderer: 'svg',
            loop: true,
            autoplay: false,
            rendererSettings: {
                progressiveLoad:false
            },
            animationData: jsons.guide
        }

        var guideAnim = bodymovin.loadAnimation(guide)
        guideAnim.play()
    } else {
        $("#guide").fadeOut()
        $('.buttons-container').removeClass('disp-none')
    }

    setTimeout(function(){
        $('#guide').fadeOut()
        $('.buttons-container').removeClass('disp-none')
    }, 4000)

    $('#guide').click(function(){
        $(this).fadeOut()
        $('.buttons-container').removeClass('disp-none')
    })
    
    $("#play-button").click(function(){
        $(this).addClass('disabled')
        hit.play()
    	playButtonAnim.play()
    	_tokensToPlay = parseInt( $('#selected-tokens').text() )
        $( document ).trigger ( 'games:onPlayGame', [ _gameID, _currencyId, _tokensToPlay ] );
    })

    var cleanButton = {
        container: document.getElementById("clear-button"),
        renderer: 'svg',
        loop: false,
        autoplay: false,
        rendererSettings: {
            progressiveLoad:false
        },
        animationData: jsons.cleanButton
    }

    var cleanButtonAnim = bodymovin.loadAnimation(cleanButton)
    
    $("#clear-button").click(function(){
        hit.play()
    	cleanButtonAnim.playSegments([0, 12], true)
    	$('#selected-tokens').text("0")
        updateTokensToPlay("0")
    	$('#play-button').addClass('disabled')
    })

	var buttonsArray = [
			button1Anim,
			button10Anim,
			button50Anim,
			button250Anim
		]

    $('.button-container').click(function(){
        hit.play()
		var value = $(this).data('value')
		var index = $('.button-container').index(this)
		var currentValue = parseInt( $('#selected-tokens').text())
        var nextValue = (currentValue + value)
		if ( nextValue > 250 ) {
			currentValue = 250
		} 
        else if ( nextValue > _userData.tokenBalance) {
			currentValue = _userData.tokenBalance
		} else {
			currentValue+=value
		}

		if (currentValue > 0){
			$('#play-button').removeClass('disabled')
		}
		$('#selected-tokens').text(currentValue)
        updateTokensToPlay(currentValue+"")
		buttonsArray[index].playSegments([0, 29], true)
    })
}

function fillDino(color){
    $('.eye_light').css({fill: color.eyeLight})
    $('.eye_base').css({fill: color.eyeBase})
    $('.dark_color_brow_d').css({fill: color.dark})
    $('.dark_color_brow_u').css({fill: color.dark})
    $('.dark_color_eye').css({fill: color.dark})
    $('.dark_color_head').css({fill: color.dark})
    $('.dark_color_nose').css({fill: color.dark})
    $('.base_color_head').css({fill: color.base})
    $('.base_color').css({fill: color.base})
    $('.base_color_left_arm').css({fill: color.base})
    $('.dark_color_detail_1').css({fill: color.dark})
    $('.dark_color_detail_2').css({fill: color.dark})
    $('.dark_color_detail_3').css({fill: color.dark})
    $('.dark_color_detail_4').css({fill: color.dark})
    $('.dark_color_detail_5').css({fill: color.dark})
    $('.dark_color_detail_6').css({fill: color.dark})
    $('.dark_color_right_arm').css({fill: color.dark})
    $('.dark_color_left_arm').css({fill: color.dark})
    $('.dark_color_tail').css({fill: color.dark})
    $('.light_color_body').css({fill: color.light})
    $('.light_color_left_wing').css({fill: color.light})
    $('.light_color_right_wing').css({fill: color.light})
    $('.dark_color_right_wing').css({fill: color.dark})
    $('.dark_color_left_wing').css({fill: color.dark})
    $('.light_color_left_hand').css({fill: color.light})
    $('.light_color_right_hand').css({fill: color.light})
    $('.base_color_body').css({fill: color.base})
    $('.darker_color_left_wing').css({fill: color.darker})
    $('.darker_color_right_wing').css({fill: color.darker})
    $('.dark_color_body').css({fill: color.dark})
    $('.light_color_mouth').css({fill: color.light})   
}

function setupColorsByID(playID) {
    switch(playID){
        case 1:
            color.base = "#07D76F"
            color.dark = "#05914B"
            color.light = "#57F0A3"
            color.eyeBase = "#17281D"
            color.eyeLight = "#2A4132"
            break
        case 2:
            color.base = "#07D76F"
            color.dark = "#05914B"
            color.darker = "#096C3B"
            color.light = "#57F0A3"
            color.eyeBase = "#17281D"
            color.eyeLight = "#2A4132"
        break
        case 3:
            color.base = "#07D76F"
            color.dark = "#05914B"
            color.light = "#57F0A3"
            color.eyeBase = "#17281D"
            color.eyeLight = "#2A4132"
            break
        case 4:
            color.base = "#F87B61"
            color.dark = "#DA5639"
            color.eyeBase = "#17281D"
            color.eyeLight = "#2A4132"
            break
        case 5:
            color.base = "#EBD03B"
            color.dark = "#CE9F09"
            color.darker = "#A58708"
            color.light = "#F7DA61"
            color.eyeBase = "#17281D"
            color.eyeLight = "#2A4132"
            break
        case 6:
            color.base = "#0C8CB1"
            color.dark = "#14576A"
            color.light = "#7FB5C5"
            color.eyeBase = "#17281D"
            color.eyeLight = "#2A4132"
            break
        case 7:
            color.base = "#664BCA"
            color.dark = "#3D20A9"
            color.eyeBase = "#17281D"
            color.eyeLight = "#2A4132"
            break
        case 8:
            color.base = "#FF5EA6"
            color.dark = "#BF4E80"
            color.darker = "#A43A62"
            color.light = "#FFA4CD"
            color.eyeBase = "#17281D"
            color.eyeLight = "#2A4132"
            break
        case 9:
            color.base = "#F87B61"
            color.dark = "#DA5639"
            color.light = "#FE9780"
            color.eyeBase = "#17281D"
            color.eyeLight = "#2A4132"
            break
        case 10:
            color.base = "#4C4C4C"
            color.dark = "#2A2A2A"
            color.darker = "#141414"
            color.light = "#818181"
            color.eyeBase = "#136F8D"
            color.eyeLight = "#50D0DF"
            break
    }
}

function detectIE() { //detected internet explorer
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older => return version number
        return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11 => return version number
        var rv = ua.indexOf('rv:');
        return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
       // Edge (IE 12+) => return version number
       return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
    }

    // other browser
    return false;
}

function setSoundSettings() {
    if(!isMuted) {$('.sound-control').attr('src','./assets/img/sound_on.png')}
    else {$('.sound-control').attr('src','./assets/img/sound_off.png')}

    soundtrack.volume = (isMuted) ? 0 : 1
    dinoSound.volume = (isMuted) ? 0 : 1
    hit.volume = (isMuted) ? 0 : 1
    whoosh.volume = (isMuted) ? 0 : 1
    prize.volume = (isMuted) ? 0 : 1
    breakSound.volume = (isMuted) ? 0 : 1
    crowd.volume = (isMuted) ? 0 : 1
}

function afterUserInfo ( userData ) {

    _userData =  userData;

    jsons.balance.layers[0].t.d.k[0].s.t = ""+_userData.pointBalance
    jsons.balance.layers[1].t.d.k[0].s.t = ""+_userData.tokenBalance

    var balance = {
        container: document.getElementById("balances-container"),
        renderer: 'svg',
        loop: false,
        autoplay: false,
        rendererSettings: {
            progressiveLoad:false
        },

        animationData: jsons.balance
    }
    var balanceAnim = bodymovin.loadAnimation(balance)
    balanceAnim.play()
    if(_userData.tokenBalance == 0){
        $('#no-tokens-screen').fadeIn()
        $('#about').fadeOut()
    }
    if(_userData.status == 0)
    {
        $('#about').fadeOut()
        $('#no-server-request').fadeIn()
        $('#t-s').text(_userData.message)
    }
    setupStartScreen()

}

function afterPlay ( data ) {

    __tokensResponse = data;
    $('#tokens-screen').fadeOut()

    setupColorsByID(__tokensResponse.playId)
    fillDino(color)
    setupGameplay()

}