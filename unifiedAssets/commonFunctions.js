/*
 * If you need to retrieve a value from the url use this function to do so. You should be able to grab the currency or
 * gameId with this. You may also grab mute status and other info that may be being held in the uri
 */
function getParameterFromURL ( property ) {

    var item;

    try {

        item = new URLSearchParams(window.location.search).get( property );

    } catch ( exception ) {

        var results = new RegExp ( '[\?&]' + property + '=([^&#]*)' ).exec ( window.location.search );

        if (results == null)
            item = null;

        else
            item = decodeURI ( results [ 1 ] ) || 0;

    }

    return item;

}