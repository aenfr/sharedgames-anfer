/**
 * This will handle the event triggered as the game loads up to gather the users balances and other pertinent data.
 */
$( document ).on ( "games:onGetUserInformation", function( event, gameId, currencyId ) {

    // is defined in clientLogic, which can be tailored to your needs. Please ensure that 'userData' is an object.
    let userData = getUserInformation ( gameId, currencyId ).done ( function ( userData ) {

        // We need to modify the main.js variables before continuing on with the game.
        updateUserInformation ( userData );

    } );

} );

/**
 * This will handle the event triggered for the play game.
 */
$( document ).on ( "games:onPlayGame", function ( event, gameId, currencyId, amount ) {

    // is defined in clientLogic.js, which can be tailored to your needs.
    let playData = postGamePlay ( gameId, currencyId, amount ).done ( function ( playData ) {

        // console.log( 'kicking off animations in event listener.' );
        updateGamePlay ( playData );

    } );

} );

/**
 * This will handle the event triggered for the after score screen so outside game balances can be updated.
 */
$( document ).on ( "games:onUpdateBalances", function ( event ) {

    // Assuming that you would like to update the balance of the user after the play has happened. You may do so here.
    // This will be defined in the clientLogic.js
    updateUserBalance ();

} );

/**
 * @param userData
 *
 * This will handle updating the balances from the data return from the api endpoint. Data should be an object, that has
 * been de-serialized from json.
 */
function updateUserInformation ( userData ) {

    // Each of the games has a slightly different process of updating the different divs and spans that contain user
    // information. We will link this back to a function where i will put all the logic that happens after the call.
    afterUserInfo ( userData );

}

/**
 * @param playData
 *
 * This method will be used to update the game after the game play event has been fired and the client logic has been run.
 */
function updateGamePlay ( playData ) {

    // Lets map the play data so that it is usable for both the old and new terminology. As a side note only should be mapped if set.
    if (playData.pointAmount)
        playData.totalPoints = playData.pointAmount;
    if (playData.tokenAmount)
        playData.totalTokens = playData.tokenAmount;
    if (playData.highestPlayId)
        playData.playId = playData.highestPlayId;

    // each of the games has a slightly different play syntax, and different animations and stuff ( out of my control ),
    // so lets just make this a connector method which will connect the client api call to the game logic. In every game
    // I will move the success function logic to a function called postPlay which can be called here which will handle
    // that games particular logic.
    afterPlay ( playData );

}